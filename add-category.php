<?php
  require 'db/db.php';

  if(isset($_POST['add-category'])){
    $name = $_POST['name'];
    $desc = $_POST['desc'];

    $sql = $conn->prepare("INSERT INTO product_category (name, `desc`) VALUES (?, ?)");
    $sql->bind_param("ss", $name, $desc);
    if($sql->execute()) {
      echo "Category Added Successfully";
    } else {
      echo "Failed to Add Category";
    }
    $sql->close();
    $conn->close();
  }

  include 'views/categories/create.view.php';
?>