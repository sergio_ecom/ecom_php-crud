<?php 
    require 'db/db.php';

    $sql = "SELECT product.*, product_category.name as category
            FROM product
            JOIN product_category
            ON product_category.id = product.cat_id;";
    $query = $conn->query($sql);
    $products = mysqli_fetch_all($query, MYSQLI_ASSOC);

    $conn->close();

    include 'views/products/index.view.php';
?>