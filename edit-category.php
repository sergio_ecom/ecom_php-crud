<?php
    require 'db/db.php';

    if (isset($_POST['update-category'])) {

        $sql = $conn->prepare("UPDATE product_category SET name=? , `desc`=?   WHERE id=?");
        $name = $_POST['name'];
        $desc = $_POST['desc'];

        $sql->bind_param("ssi", $name, $desc, $_GET["id"]);
        $sql->execute();
      
    }

    $sql = $conn->prepare("SELECT * FROM product_category WHERE id=?");
    $sql->bind_param("i",$_GET["id"]);
    $sql->execute();
    $result = $sql->get_result();
    if ($result->num_rows > 0) {
        $category = $result->fetch_assoc();
    } else {
        header('Location: categories.php');
    }

    include 'views/categories/edit.view.php';

?>
