<?php
    require 'db/db.php';

    if (isset($_POST['update-product'])) {

        $sql = $conn->prepare("UPDATE product SET name=? , cat_id=? , short_desc=? , long_desc=?   WHERE id=?");
        $name = $_POST['name'];
        $category_id = $_POST['cat'];
        $short_desc = $_POST['sdesc'];
        $long_desc = $_POST['ldesc'];
      
        $sql->bind_param("sissi",$name, $category_id, $short_desc, $long_desc, $_GET["id"]);
        $sql->execute();
      
    }

    $sql = $conn->prepare("SELECT * FROM product WHERE id=?");
    $sql->bind_param("i",$_GET["id"]);
    $sql->execute();
    $result = $sql->get_result();
    if ($result->num_rows > 0) {
        $product = $result->fetch_assoc();
    } else {
        header('Location: products.php');
    }

    $sql = "SELECT * FROM product_category";
    $query = $conn->query($sql);
    $categories = mysqli_fetch_all($query,MYSQLI_ASSOC);
    $conn->close();

    include 'views/products/edit.view.php';

?>
