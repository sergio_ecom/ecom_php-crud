<?php
    require 'db/db.php';

	$sql = $conn->prepare("DELETE  FROM product_category WHERE id=?");
	$sql->bind_param("i", $_GET["id"]);
	$sql->execute();
	$sql->close();
	$conn->close();

	header('location:categories.php');

?>