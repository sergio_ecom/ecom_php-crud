<?php
  require 'db/db.php';

  $sql = "SELECT * FROM product_category";
  $query = $conn->query($sql);
  $categories = mysqli_fetch_all($query, MYSQLI_ASSOC);
  $conn->close();

  include 'views/categories/index.view.php';
?>