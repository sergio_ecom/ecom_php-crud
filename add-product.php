<?php
    require 'db/db.php';

    if (isset($_POST['add-product'])) {

        $sql = $conn->prepare("INSERT INTO product (cat_id,name,short_desc,long_desc) VALUES (?,?,?,?)");
        $name = $_POST['name'];
        $short_desc = $_POST['sdesc'];
        $long_desc = $_POST['ldesc'];
        $category_id= $_POST['cat'];
      
        $sql->bind_param("isss", $category_id, $name, $short_desc, $long_desc);
        $sql->execute();

        $sql->close();
    }

    $sql = "SELECT * FROM product_category";
    $query = $conn->query($sql);
    $categories = mysqli_fetch_all($query,MYSQLI_ASSOC);
    $conn->close();

    include 'views/products/create.view.php';

?>
