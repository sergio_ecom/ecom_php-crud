<?php include 'includes/header.php'; ?>

  <table id="categories">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($categories as $category): ?>
        <tr>
          <th><?= $category['id']; ?></th>
          <td><?= $category['name']; ?></td>
          <td><?= $category['desc']; ?></td>
          <td>
            <a href="edit-category.php?id=<?=$category['id'] ?>" role="button">Edit</a>
            <a href="delete-category.php?id=<?=$category['id'] ?>">Delete</a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <a href="add-category.php">Add Category</a>
  
<?php include 'includes/footer.php';?>