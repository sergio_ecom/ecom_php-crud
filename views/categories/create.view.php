<?php include 'includes/header.php'; ?>

  <div class="">
    <h3>Create Category</h3>
    <hr>
    <form method="post" value="">
      <label for="name">Category name</label>
      <input type="text" name="name" placeholder="Category Name">
        <br>
      <label for="desc"> Description</label>
      <input type="text" name="desc" placeholder="Category Description">
        <br>
      <button type="submit" name="add-category">Save</button>
    </form>
  </div>

<?php include 'includes/footer.php';?>