<?php include 'includes/header.php'; ?>

    <div class="">
        <h3>Create Product</h3>
        <hr>
        <form method="post" value="">
            <label for="name">Product name</label>
            <input type="text" name="name" placeholder="Product Name" value="<?= $product['name']?>">
            <br>
            <label for="sdesc">Product Short Description</label>
            <input type="text" name="sdesc" placeholder="Product Short Description" value="<?= $product['short_desc']?>">
            <br>
            <label for="ldesc">Product Long Description</label>
            <textarea name="ldesc" cols="30" rows="1" placeholder="Product Long Description"><?= $product['long_desc']?></textarea>
            <br>
            <label for="cat">Category</label>
            <select name="cat" id="cat">
                <?php foreach ($categories as $category): ?>
                    <?php if ($category['id'] == $product['category_id']): ?>
                        <option value="<?= $category['id']; ?>" selected><?= $category['name']; ?></option>
                    <?php else: ?>
                        <option value="<?= $category['id']; ?>"><?= $category['name']; ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
            <br>
            <button type="submit" name="update-product">Save</button>
        </form>
    </div>

<?php include 'includes/footer.php'; ?>
