<?php include 'includes/header.php'; ?>

    <table id="categories">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Category Name</th>
            <th>Short Description</th>
            <th>Long Description</th>
            <th>Actions</th>
        </tr>
        </thead>


            <tbody>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <th><?= $product['id']; ?></th>
                        <td><?= $product['name']; ?></td>
                        <td><?= $product['category']; ?></td>
                        <td><?= $product['short_desc']; ?></td>
                        <td><?= $product['long_desc']; ?></td>
                        <td>
                            <a href="edit-product.php?id=<?=$product['id'] ?>" role="button">Edit</a>
                            <a href="delete-product.php?id=<?=$product['id'] ?>" role="button">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
    </table>
    <br>

    <a href="add-product.php">Add Products</a>

<?php include 'includes/footer.php'; ?>